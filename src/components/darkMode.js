import theme from "styled-theming"

export const LinkColor = theme("mode", {
  light: "#1f1f22",
  dark: "#fff",
})

export const Backgrounds = theme("mode", {
  light: "#fff",
  dark: "#1e2227",
})

export const BorderColor = theme("mode", {
  light: "rgb(245, 243, 247)",
  dark: "rgb(27, 27, 27)",
})
