import React from "react"
import styled from "styled-components"

import { Link } from "gatsby"
import { rhythm } from "../utils/typography"
import { LinkColor, Backgrounds, BorderColor } from "./darkMode"

import normalLogo from "../assets/images/remote-logo.svg"
import whiteLogo from "../assets/images/logo_white.svg"

const HeaderContainer = styled.header`
  position: fixed;
  left: 0;
  top: 0;
  z-index: 99;
  display: flex;
  justify-content: center;
  width: 100%;
  height: 60px;
  border-bottom: 1px solid ${BorderColor};
  background-color: ${Backgrounds};
`

const InnerHeader = styled.div`
  width: 100%;
  margin: 0 auto;
  padding: 0 ${rhythm(3 / 4)};
  display: flex;
  align-items: center;
  justify-content: space-between;
`

const HeaderNav = styled.nav`
  & a {
    margin-left: 35px;
    font-size: 14px;

    @media screen and (max-width: 475px) {
      font-size: 13px;
      margin-left: 15px;
    }
  }
`

const NavLink = styled.a`
  color: ${LinkColor};

  @media screen and (max-width: 375px) {
    display: none;
  }
`

const NavCta = styled.a`
  padding: 0.5rem 1.07143rem;
  border-radius: 4px;
  background-image: none;
  background-color: #2449f9;
  color: #fff;
  box-shadow: none;
  text-decoration: none;
`

const LogoImg = styled.img`
  width: 110px;
  margin: 0;
  display: block;

  @media screen and (max-width: 475px) {
    width: 90px;
  }
`

class Header extends React.Component {
  render() {
    const { title, mode } = this.props

    return (
      <HeaderContainer>
        <InnerHeader>
          <Link
            style={{
              boxShadow: `none`,
            }}
            to={`/`}
          >
            <LogoImg
              src={mode === "light" ? normalLogo : whiteLogo}
              alt={title}
            />
          </Link>
          <HeaderNav>
            <NavLink href="https://www.remote.com/jobs/browse" target="_blank">
              Find remote jobs
            </NavLink>
            <NavCta href="https://www.remote.com/recruit" target="_blank">
              Post a job
            </NavCta>
          </HeaderNav>
        </InnerHeader>
      </HeaderContainer>
    )
  }
}

export default Header
