import React from "react"
import styled from "styled-components"
import theme from "styled-theming"
import { Link, graphql } from "gatsby"
import { DiscussionEmbed } from "disqus-react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import { rhythm } from "../utils/typography"
import { LinkColor } from "../components/darkMode"
import { getDarkModeCookieValue } from "../components/helpers/cookies"

const TitleColor = theme("mode", {
  light: "#1f1f22",
  dark: "#fff",
})

const PostTitle = styled.h1`
  line-height: 1.5em;
  letter-spacing: -0.03em;
  margin: 0;
  font-size: 48px;
  color: ${TitleColor};
  text-align: left;
  font-weight: 600;

  @media screen and (max-width: 475px) {
    font-size: 38px;
  }
`

const DateText = styled.small`
  opacity: 0.52;
  color: #7d7d7d;
  display: block;
  margin-bottom: ${rhythm(1)};
`

const ContentColor = theme("mode", {
  light: "#252525",
  dark: "#fff",
})

const PostImage = styled.img`
  display: block;
  max-width: 400px;
  height: auto;
  margin: 0 auto 30px;

  @media screen and (max-width: 450px) {
    max-width: 100%;
  }
`

const PostContent = styled.div`
  & * {
    color: ${ContentColor};
  }

  & a {
    color: ${LinkColor};
  }
`

const NavigationLinks = styled(Link)`
  color: ${LinkColor};
`

class BlogPostTemplate extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      mode: "light",
    }
  }

  componentDidMount() {
    this.setState({ mode: getDarkModeCookieValue() })
  }

  render() {
    const post = this.props.data.markdownRemark
    const siteTitle = this.props.data.site.siteMetadata.title
    const { previous, next } = this.props.pageContext

    const disqusShortname = "remote-com"
    const disqusConfig = {
      identifier: post.id,
      title: post.frontmatter.title,
    }

    return (
      <Layout location={this.props.location} title={siteTitle}>
        <SEO
          title={post.frontmatter.title}
          description={post.frontmatter.description || post.excerpt}
        />
        {post.frontmatter.image && (
          <PostImage
            src={require(`../assets/images/${post.frontmatter.image}`)}
            alt={post.frontmatter.title}
          />
        )}
        <PostTitle>{post.frontmatter.title}</PostTitle>
        <DateText>{post.frontmatter.date}</DateText>
        <PostContent dangerouslySetInnerHTML={{ __html: post.html }} />
        <hr
          style={{
            marginBottom: rhythm(1),
          }}
        />

        <ul
          style={{
            display: `flex`,
            flexWrap: `wrap`,
            justifyContent: `space-between`,
            listStyle: `none`,
            padding: 0,
          }}
        >
          <li>
            {previous && (
              <NavigationLinks to={previous.fields.slug} rel="prev">
                ← {previous.frontmatter.title}
              </NavigationLinks>
            )}
          </li>
          <li>
            {next && (
              <NavigationLinks to={next.fields.slug} rel="next">
                {next.frontmatter.title} →
              </NavigationLinks>
            )}
          </li>
        </ul>
        {process.env.NODE_ENV === "production" && (
          <DiscussionEmbed shortname={disqusShortname} config={disqusConfig} />
        )}
      </Layout>
    )
  }
}

export default BlogPostTemplate

export const pageQuery = graphql`
  query BlogPostBySlug($slug: String!) {
    site {
      siteMetadata {
        title
        author
      }
    }
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      excerpt(pruneLength: 160)
      html
      frontmatter {
        title
        date(formatString: "MMMM DD, YYYY")
        description
        image
      }
    }
  }
`
